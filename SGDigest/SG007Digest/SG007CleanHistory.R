rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSG007History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/SGDigest/SG007Digest/FTPSG007Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1)
LTCUTOFF = 0.001
COUNTER = 0
THRESHOLD = 10
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/1)+1))
}

checkErrata = function(row,ts)
{
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent so no more mails'))
		return()
	}
	{
	if((!(is.finite(as.numeric(row[2])))) || (abs(as.numeric(row[2])) < LTCUTOFF))
	{
		COUNTER <<- COUNTER + 1
		if(COUNTER < THRESHOLD)
			return()
		FIREERRATA <<- 0
		subj = paste('SG-007X down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("SG-007X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent'))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "SG-007X"',sep = "")
		system(command)
		print('Twilio message fired')
		COUNTER <<- 0
		recordTimeMaster("SG-007X","TwilioAlert",as.character(row[1]))
	}
	else
	{
		COUNTER <<- 0
	}
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
	idxvals = match(currcolnames,colnames)
	idxvals = idxvals[complete.cases(idxvals)]
	tm = as.POSIXlt(as.character(dataread[,1]),format="%m-%d-%Y %H:%M:%OS")
	tm = as.POSIXlt(tm,format="%Y-%m-%d %H:%M:%OS")
	tm = as.character(tm)
	dataread[,1] = tm
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = pathwritemon
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname," ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
			FIREERRATA <<- c(1)
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,47]))
	checkErrata(pass,idxts)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
	}
  recordTimeMaster("SG-007X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/SG007 Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[SG-007X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'SG007.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('')
idxtostart = c(1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[SG-007X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1])
		idxtostart[1] = match(lastrecordeddate[1],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[idxtostart],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[SG-007X]"
{
	if(idxtostart[1] < length(days))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days is',length(days)))
		for(x in idxtostart[1] : length(days))
		{
 		 stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days[x])
		print('Meter 1 DONE')
	}
	else if( (!(idxtostart[1] < length(days))) )
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
errataCheckFirst = 0
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	print('Done match')
	if(length(daysnew) == length(days))
	{
	print('condn true')
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		print('sleeping')
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	print('check done')
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,errataCheckFirst)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew
	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	write(c(as.character(daysnew1[length(daysnew1)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
gc()
errataCheckFirst= 1
}
print('Out of loop')
sink()
