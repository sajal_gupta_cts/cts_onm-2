#2 718 
rm(list=ls(all =TRUE))

library(ggplot2)

pathWrite <- "/home/admin/Jason/cec intern/results/718/"
result <- read.csv("/home/admin/Jason/cec intern/results/718/[718]_summary.csv")

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
result <- result[,-1]
colnames(result) <- c("date","da","pts","spm10","spm10ptsabove5")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))


date <- result[,1]
last <- tail(result[,1],1)    #date[length(date)] works
first <- date[1]   #date[1]

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
da1 <- dagraph + geom_line(size=0.5)
da1 <- da1 + theme_bw()
da1 <- da1 + expand_limits(x=date[1],y=0)
da1 <- da1 + scale_y_continuous(breaks=seq(0, 115, 10))
#da1 <- da1 + scale_x_date(date_breaks = "10 days",date_labels = "%b %d")
da1 <- da1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
da1 <- da1 + ggtitle(paste("[718] Data Availability","\n from ",date[1]," to ",last))
da1 <- da1 + theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
da1 <- da1 + theme(axis.title.x = element_blank(),  panel.grid.minor = element_blank())
da1 <- da1 + theme(axis.text.x = element_text(size=11))
da1 <- da1 + theme(axis.text.y = element_text(size=11))
da1 <- da1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))
da1 <- da1 + theme(plot.title=element_text(margin=margin(0,0,7,0)))
da1 <- da1 + annotate("text",label = paste0("Average data availability = ", round(mean(result[,2], na.rm = TRUE),1),"%"),size=5.4,
                      x = as.Date(date[round(0.65*length(date))]), y= 62.5)
da1 <- da1 + annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 5.4,
                      x = as.Date(date[round(0.65*length(date))]), y= 57)
da1 <- da1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

da1

ggsave(da1,filename = paste0(pathWrite,"718_DA_LC.pdf"), width = 7.92, height = 5)
##

sp <- ggplot(result, aes(x=date,y=result[,5]))+ylab("SMP10 Points > 5 W/m2")
sp <- sp + geom_line(size=0.5)
sp <- sp + theme_bw()
sp <- sp + expand_limits(x=date[1],y=0)
sp <- sp + scale_y_continuous(breaks=seq(0, 700, 100))
sp <- sp + scale_x_date(date_breaks = "5 days",date_labels = "%b %d")
sp <- sp + ggtitle(paste("[718] SMP10 Points > 5W/M2 \n From ",date[1]," to ",last))
sp <- sp + theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
sp <- sp + theme(axis.title.x = element_blank())
sp <- sp + theme(axis.text.x = element_text(size=11))
sp <- sp + theme(axis.text.y = element_text(size=11))
sp <- sp + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))
sp <- sp + theme(plot.title=element_text(margin=margin(0,0,7,0)))
sp


