rm(list=ls(all =TRUE))

###     IN-714 DATA EXTRACTION       ###
pathRead <- "~/intern/Data/[714]/2017"
pathWrite <- "C:/Users/talki/Desktop/cec intern/results/714/[714]_summary.txt"
pathWrite2 <- "C:/Users/talki/Desktop/cec intern/results/714/[714]_summary.csv"
setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)

nameofStation <- "714"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col8 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = col7[10^6]= col8[10^6]=0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability0
  pts <- length(temp[,2])
  col3[index] <- pts
  
  #gsi/ irradiation
  gsi <- as.numeric(temp[,3])     #5 is for usual wallgraph. 4 for paper
  col4[index] <- sum(gsi)/60000
  
  #pac
  col5[index] =  sum(abs(temp[,71]))
  
  print(paste(i, "done"))
  
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col7 <- col7[1:(index-1)]
col8 <- col8[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA              #states T/F
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA
col8[is.na(col8)] <- NA

print("Starting to save..")

result <- cbind(col0,col1,col2,col3,col4,col5)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI", "Pac")

for(x in c(3,5,6)){
  result[,x] <- round(as.numeric(result[,x]),1)
  #print(result[,x])
}

rownames(result) <- NULL
result <- data.frame(result)    
#result <- result[-length(result[,1]),]   #removing last row 
dataWrite <- result
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")
write.csv(result,pathWrite2, na= "", row.names = FALSE)



