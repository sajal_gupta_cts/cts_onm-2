rm(list=ls())
errHandle = file('/home/admin/Logs/LogsIN050CMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN050CDigest/summaryFunctions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/common/math.R')
RESETHISTORICAL=0
reorderStnPaths = c(
  188,96, #WMS Entries
  22,23,26,52,53,67,89,90,92,93,94,95,107,108,126,127,138,158,185,186,187,193,198,204,210,216, #MFM Entries
  1,12,15,16,17,18,19,20,21,2,3,4,5,6,7,8,9,10,11,13,14,# ACH Subset
  24,25, #AMB subset
  27,38,45,46,47,48,49,50,51,28,29,30,31,32,33,34,35,36,37,39,40,41,42,43,44,#AMD Subset
  55,59,60,61,62,63,64,65,66,56,57,58, #ANK Subset
  68,79,82,83,84,85,86,87,88,69,70,71,72,73,74,75,76,77,78,80,81, # ANM Subset
  97,99,100,101,102,103,104,105,106,98, #C1 Subset
  110,118,119,120,121,122,123,124,125,111,112,113,114,115,116,117, #C234 Subset
  129,130,131,132,133,134,135,136,137, #DS Subset
  140,141,142,143, #Greygo subset
  144,145,146,147, #Insp Subset
  148,149,150,151,152,153, #Loom Subset
  154,155, #Shed100 Subset
  156,157, #SL Subset
  159,170,178,179,180,181,182,183,184,160,161,162,163,164,165,166,167,168,169,171,172,173,174,175,176,177, #SSR Subset
  189,190,191,192, #TP1 Subset
  194,195,196,197, #TP-2 Subset
  201,199,200, #Warp subset
  202,203, #X12 Subset
  205,206,207,208,209, #YG Subset
  212,213,214,215 #YW Subset
)
source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
require('lubridate')

METERNICKNAMES = readLines("/home/admin/CODE/IN050CDigest/order.txt")
METERNICKNAMES = gsub("_","-",METERNICKNAMES)
METERNICKNAMES = gsub("Inverter","Inv",METERNICKNAMES)

METERACNAMES = readLines("/home/admin/CODE/IN050CDigest/order.txt") 

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}

COD = as.Date("2019-01-08", format = "%Y-%m-%d")
currDate <- as.Date(format(Sys.time(), "%Y-%m-%d"), format = "%Y-%m-%d")
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-050C","m")
pwd = 'CTS&*(789'
referenceDays = NA

extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}

analyseDays = function(days,ref)
{
	if(length(days) == length(ref))
		return(days)
	days2 = extractDaysOnly(days)
	daysret = unlist(rep(NA,length(ref)))
	idxmtch = match(days2,ref)
	idxmtch2 = idxmtch[complete.cases(idxmtch)]
	if(length(idxmtch) != length(idxmtch2))
	{
		print(".................Missmatch..................")
		print(days2)
		print("#########")
		print(ref)
		print("............................................")
		return(days)
	}
	daysret[idxmtch] = as.character(days)
	if(!(is.na(daysret[length(daysret)])))
		return(daysret)
	else
		return(days)
}
performBackWalkCheck = function(day)
{
	path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]"
	retval = 0
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	pathprobe = paste(path,yr,yrmon,sep="/")
	stns = dir(pathprobe)
	print(stns)
	idxday = c()
	for(t in 1 : length(stns))
	{
		pathdays = paste(pathprobe,stns[t],sep="/")
		print(pathdays)
		days = dir(pathdays)
		dayMtch = days[grepl(day,days)]
		if(length(dayMtch))
		{
			idxday[t] = match(dayMtch,days)
		}
		print(paste("Match for",day,"is",idxday[t]))
	}
	idxmtch = match(NA,idxday[t])
	if(length(unique(idxday))==1 || length(idxmtch) < 5)
	{
		print('All days present ')
		retval = 1
	}
	return(retval)
}
sendMail= function(pathall)
{
  filenams = c()
  body = ""
	body = paste(body,"Site Name: Arvind Mills",sep="")
	body = paste(body,"\n\nLocation: Ahmedabad, India")
	body = paste(body,"\n\nO&M Code: IN-050")
	body = paste(body,"\n\nSystem Size:",TOTAL_INSTCAPM)
	body = paste(body,"\n\nNumber of Energy Meters:",NOMETERS)
	body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar / 320Wp / 28215 ; JA Solar / 325 Wp / 17648 ; JA Solar / 330 Wp / 600")
	body = paste(body,"\n\nInverter Model / Nos: 50 KW / 36 ; 69 KW / 146 kW")
	body = paste(body,"\n\nSite COD:",COD)
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(currDate - COD))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round(time_length(difftime(currDate, COD), "years"), 1)))
	body = paste(body,"\n\n________________________________________________\n\n")
    body = paste(body,"FULL SITE")
    body = paste(body,"\n________________________________________________\n")
	bodyac = body
	body = ""
	TOTALGENCALC = 0
	MYLD = c()
	
	acname = METERNICKNAMES
	acnames = readLines("/home/admin/CODE/IN050CDigest/FullNames.txt")
	noInverters = NOINVS
	InvReadings = unlist(rep(NA,noInverters))
	InvAvail = unlist(rep(NA,noInverters))
	globMtYld = globMtVal = globMtname = c()
	idxglobMtYld = 1
	GTI = NA
	for(t in 1 : length(pathall))
  {
	type = 0
	meteridx = t
	if(grepl("MFM",pathall[t]) || grepl("WMS",pathall[t]))
		type = 1
	if(type == 0)
	{
		meternames = METERACNAMES[29:length(METERACNAMES)]
		pathsplit = unlist(strsplit(pathall[t],"/"))
		InvName = pathsplit[10]
		meteridx  = grep(InvName,meternames) 
	}
	path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	offsetnam = 0
	if(type == 0)
	  offsetnam = 2+NOMETERS
	sub_stn = unlist(strsplit(pathall[t],"/"))
	sub_stn = gsub("_","-",sub_stn[10])
	if(grepl("Inverter",sub_stn))
	  sub_stn = gsub("Inverter","Inv",sub_stn)
	idx = grep(sub_stn,METERNICKNAMES)
	filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx + offsetnam],".txt",sep="")
	print("---------------------")
	print(path)
	print(filenams[t])
	print("---------------------")
	if(type == 1)
	{
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,acnames[idx])
	body = paste(body,"\n________________________________________________\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	{
	if(grepl("MFM",pathall[t]))
	{
		if(grepl("AMB", pathall[t]))
		{
            body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
            body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
            TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
            body = paste(body,"Yield-1 [kWh/kWp]:",as.character(round(dataread[1,5]/1000,2)),"\n\n")
            body = paste(body,"Yield-2 [kWh/kWp]:",as.character(round(dataread[1,6]/1000,2)),"\n\n")
            globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
            globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
            globMtname[idxglobMtYld] = acnames[idx]
            idxglobMtYld = idxglobMtYld + 1
            body = paste(body,"PR-1 [%]:",as.character(round(dataread[1,7]/1000,1)),"\n\n")
            body = paste(body,"PR-2 [%]:",as.character(round(dataread[1,8]/1000,1)),"\n\n")
            body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]),"\n\n")
            body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
            body = paste(body,"Grid Availability [%]:",as.character(dataread[1,11]),"\n\n")
            body = paste(body,"Plant Availability [%]:",as.character(dataread[1,12]))
		}
		else
		{
	body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
	globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
	globMtname[idxglobMtYld] = acnames[idx]
	idxglobMtYld = idxglobMtYld + 1
	body = paste(body,"PR-1 [%]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"PR-2 [%]:",as.character(dataread[1,8]),"\n\n")
	body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]/1000),"\n\n")
	body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
	body = paste(body,"Grid Availability [%]:",as.character(dataread[1,11]),"\n\n")
	body = paste(body,"Plant Availability [%]:",as.character(dataread[1,12]))
	}
	}
	else
	{
		body = paste(body,"GHI [kWh/m^2]:",as.character(dataread[1,3]),"\n\n")
		GTI = as.numeric(dataread[1,3])
		if(grepl("Bot",pathall[t]))
		{
		  body = paste(body,"Tmod [C]:",as.character(dataread[1,5]),"\n\n")
		  body = paste(body,"Tmod solar hours[C]:",as.character(dataread[1,7]))
		}
		else
		{
		  body = paste(body,"Tamb [C]:",as.character(dataread[1,4]),"\n\n")
		  body = paste(body,"Tamb solar hours [C]:",as.character(dataread[1,6]))
		}
	}
	}
	next
  }
	InvReadings[meteridx] = as.numeric(dataread[1,7])
	InvAvail[meteridx]= as.numeric(dataread[1,10])
	MYLD[(meteridx)] = as.numeric(dataread[1,7])
	}
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,"Inverters")
	body = paste(body,"\n________________________________________________")
	MYLDCPY = MYLD
	MYLD = MYLD[complete.cases(MYLD)]
	addwarn = 0
	if(length(MYLD))
	{
		addwarn = 1
		sddev = round(sdp(MYLD),2)
		meanyld = mean(MYLD)
		print("Printing mean yield")
		print(meanyld)
		print(sdp(MYLD))
		covar = round((sdp(MYLD)*100/meanyld),1)
	}
	MYLD = MYLDCPY
	for(t in 1 : noInverters)
	{
		body = paste(body,"\n\n Yield ",acname[t+2+NOMETERS],": ",as.character(InvReadings[t]),sep="")
		if( addwarn && is.finite(covar) && covar > 5 &&
		(!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
		{
			body = paste(body,">>> Inverter outside range")
		}
	}
	body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
	for(t in 1 : noInverters)
	{
	  body = paste(body,"\n\n Inverter Availability ",acname[(t+2+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
	}
	bodyac = paste(bodyac,"\n\nEnergy Generated FULL SITE [kWh]:",sum(globMtVal))
  bodyac = paste(bodyac,"\n\nYield FULL SITE [kWh/kWp]:",
                 round(sum(globMtVal)/TOTAL_INSTCAPM,2))
  bodyac = paste(bodyac,"\n\nGHI [kWh/m2]:",GTI)
  bodyac = paste(bodyac,"\n\nPerformance Ratio FULL SITE [%]:",
                 round(sum(globMtVal)/(TOTAL_INSTCAPM*GTI)*100,1),"\n\n")
	bodyac = paste(bodyac,"________________________________________________\n\n")
	for(t in 1 : length(globMtname))
	{
		if(globMtname[t] == "Amber Warehouse-MFM1")
		{
      bodyac = paste(bodyac,"Yield",globMtname[t],"[kWh/kWp]:",
                     as.character(round(globMtYld[t]/1000,2)),"\n\n")
		}
		else{
		 bodyac = paste(bodyac,"Yield",globMtname[t],"[kWh/kWp]:",
			 as.character(globMtYld[t]),"\n\n")
			 }
	}
	sdm =round(sdp(globMtYld)/1000,2)

  diff = round(mean(globMtYld)/100,1)
  mglobMtYld = mean(globMtYld)/1000
  covarm = round((sdm/mglobMtYld),1)
  print("Printing covarm")
  covarm = covarm - diff
	
	bodyac = paste(bodyac,"Stdev/COV Yields:",sdm,"/",covarm,"[%]")
	body = paste(bodyac,body,sep="")
	body = gsub("\n ","\n",body)
	firecond = lastMailDate(paste('/home/admin/Start/MasterMail/IN-050C_Mail.txt',sep=""))
  	if (currday == firecond)
    	return()
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-050C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
recordTimeMaster("IN-050C","Mail",currday)
}


path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-050C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-050C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-050C]'

if(RESETHISTORICAL)
{
	command = paste("rm -rf",path2G)
	system(command)
	command = paste("rm -rf",path3G)
	system(command)
	command = paste("rm -rf",path4G)
	system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-050C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-YG_I5-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 2 : length(years)) # 2018 data isn't valid so start from 2018
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		stns = dir(pathmon)
		stns = stns[reorderStnPaths]
		dunmun = 0
		for(t in 1 : length(stns))
		{
			type = 1
			if(grepl("MFM",stns[t]))
				type = 0
			if(grepl("WMS",stns[t]))
			  type = 2
			pathmon1 = paste(pathmon,stns[t],sep="/")
		  days = dir(pathmon1)
		  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			if(!file.exists(path2Gmon1))
			{	
				dir.create(path2Gmon1)
			}

		if(length(days)>0)
		{
		for(z in 1 : length(days))
		{
			if(ENDCALL == 1)
				break
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon1,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon1,days[z],sep="/")
			if(RESETHISTORICAL)
			{
				secondGenData(pathfinal,path2Gfinal,type)
			}
			if(days[z] == stopDate)
			{
				ENDCALL = 1
				print('Hit end call')
				next
			}
		}
		}
		dunmun = 1
		if(ENDCALL == 1)
			break
	}
	if(ENDCALL == 1)
	break
}
if(ENDCALL == 1)
break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
	recipients = getRecipients("IN-050C","m")
	recordTimeMaster("IN-050C","Bot")
	years = dir(path)
	noyrs = length(years)
	path2Gfinalall = vector('list')
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		checkdir(path2Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			stns = dir(pathmon)
			if(length(stns) < 30) ## Need to make change according to no.of  substations
			{
				print('Station sync issue.. sleeping for an hour')
				Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
				stns = dir(pathmon)
			}
			stns = stns[reorderStnPaths]
			for(t in 1 : length(stns))
			{
				newlen = (y-startmn+1) + (x-prevx)
				print(paste('Reset newlen to',newlen))
				type = 1
				if(grepl("MFM",stns[t]))
					type = 0
				if(grepl("WMS",stns[t]))
					type = 2
			  pathmon1 = paste(pathmon,stns[t],sep="/")
			  days = dir(pathmon1)
			  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			chkcopydays = days[grepl('Copy',days)]
			if(!file.exists(path2Gmon1))
			{
			dir.create(path2Gmon1)
			}
			
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			days = days[complete.cases(days)]
			if(length(days)>0)
			{
			if(t==1){
			referenceDays <<- extractDaysOnly(days)
			}else{
			days = analyseDays(days,referenceDays)}
			}
			nodays = length(days)
			if(y > startmn)
			{
				z = prevz = 1
			}
			if(nodays > 0)
			{
			startz = prevz
			if(startz > nodays)
				startz = nodays
			for(z in startz : nodays)
			{
				if(t == 1)
				{
					path2Gfinalall[[newlen]] = vector('list')
				}
				if(is.na(days[z]))
					next
				pathdays = paste(pathmon1,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon1,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal,type)
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				if(is.na(days[z]))
				{
					print('Day was NA, do next in loop')
					next
				}
			  SENDMAILTRIGGER <<- 1
				if(t == 1)
				{
					splitstr =unlist(strsplit(days[z],"-"))
					daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
					print(paste("Sending",daysSend))
					if(performBackWalkCheck(daysSend) == 0)
					{
						print("Sync issue, sleeping for an 1 hour")
						Sys.sleep(3600) # Sync issue -- meter data comes a little later than
					   	             # MFM and WMS
					}
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
				newlen = newlen + 1
				print(paste('Incremented newlen by 1 to',newlen))
			}
			}
			}
		}
	}
	if(SENDMAILTRIGGER)
	{
	print('Sending mail')
	print(paste('newlen is ',newlen))
	for(lenPaths in 1 : (newlen - 1))
	{
		if(lenPaths < 1)
			next
		print(unlist(path2Gfinalall[[lenPaths]]))
		pathsend = unlist(path2Gfinalall[[lenPaths]])
		sendMail(pathsend)
	}
	SENDMAILTRIGGER <<- 0
	newlen = 1
	print(paste('Reset newlen to',newlen))

	}

	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
